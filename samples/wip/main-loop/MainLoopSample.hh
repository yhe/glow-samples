#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class MainLoopSample : public glow::glfw::GlfwApp
{
public:
    MainLoopSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mMainLoop;

    double mUpdateRate = 60;
    float mRuntime = 0.0f;
    bool mAnimate = true;
    bool mVSync = true;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
    void update(float elapsedSeconds) override;
};
