#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class MassivePointsSample : public glow::glfw::GlfwApp
{
public:
    MassivePointsSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mPoints;

    int mPointsPerSide = 128;

protected:
    void init() override;
    void render(float elapsedSeconds) override;

public:
    void rebuild();
};
