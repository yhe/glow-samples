layout(points) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 vsNormal[];
in vec3 vsTangent[];

out vec3 gsColor;
out vec2 gsVelocity;

uniform sampler2D uNoise;
uniform sampler2D uTrampleTexture;

#include "grass_globals.glsl"

void main()
{
    // Get blade base position
    const vec3 pos = gl_in[0].gl_Position.xyz;

    // Read trample value
    const vec4 trampleValue = texture(uTrampleTexture, getTrampleUv(pos.xz)).rgba;

    // Sample perlin noise map at various frequencies
    float randomColor = texture(uNoise, vec2(pos.x, pos.z) * .75).r;
    float randomHeight = texture(uNoise, vec2(pos.x, pos.z) * .5).r;
    float randomWind = texture(uNoise, vec2(pos.x, pos.z) * .25).r;

    // Calculate vertex offsets
    //const vec3 bitangent = cross(vsTangent[0], vsNormal[0]);
    vec3 biasedNormal = normalize(mix(vsNormal[0], uGrassInfo.cameraUp, uGrassInfo.cameraUpBias));
    vec3 tangentOffset = vsTangent[0] * uGrassInfo.bladeThickness;
    vec3 heightOffset = biasedNormal * (uGrassInfo.bladeLength * randomHeight);
    vec3 windOffset = vsTangent[0] * (uGrassInfo.windStrength * randomWind * sin((pos.x + pos.z + uGrassInfo.runtime) * uGrassInfo.windSpeed));
    vec3 combinedVerticalOffset = (heightOffset + windOffset) * (trampleValue.a < .5 ? 0.05 : 1);

    // Calculate approximated fragment velocity for TAA
    vec4 hdc = uGrassInfo.cleanVp * vec4(pos, 1.0);
    vec4 prevHdc = uGrassInfo.prevCleanVp * vec4(pos, 1.0);
    gsVelocity = getFragmentVelocity(hdc, prevHdc);

    // Output the triangle
    gl_Position = uGrassInfo.vp * vec4(pos - tangentOffset, 1);
    gsColor = bladeBottomColor;
    EmitVertex();

    gl_Position = uGrassInfo.vp * vec4(pos + combinedVerticalOffset, 1);
    gsColor = (randomColor > .5 ? bladeTopColorA : bladeTopColorB);
    EmitVertex();

    gl_Position = uGrassInfo.vp * vec4(pos + tangentOffset, 1);
    gsColor = bladeBottomColor;
    EmitVertex();
}
