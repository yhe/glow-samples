#glow pipeline opaque

#include <glow-pipeline/pass/opaque/opaquePass.glsl>

in vec3 gsColor;
in vec2 gsVelocity;

void main()
{
     outputOpaqueGeometry(gsColor, gsVelocity);
}
