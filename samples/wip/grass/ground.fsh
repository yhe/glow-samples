#glow pipeline opaque

#include <glow-pipeline/pass/opaque/opaquePass.glsl>

uniform vec3 uLightDir = normalize(vec3(.2, 1, .3));

in vec3 vPosition;
in vec3 vNormal;

void main() {
    outputOpaqueGeometry(vec3(.545, .271, .075) * (0.2 + 0.8 * max(0, dot(vNormal, uLightDir))));
}
