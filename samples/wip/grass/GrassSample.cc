#include "GrassSample.hh"

#include <glow/gl.hh>

#include <random>

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/Texture3D.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/data/TextureData.hh>

#include <glow/util/DefaultShaderParser.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/assimp/Importer.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>
#include <glow-extras/pipeline/RenderPipeline.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>

#include <GLFW/glfw3.h>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

namespace
{
bool volumeGenerationRequested = false;
void TW_CALL setGenerateVolume(void*)
{
    // Generate volume next frame
    volumeGenerationRequested = true;
}
}

void GrassSample::GenerateVolume()
{
    GLOW_SCOPED(disable, GL_DEPTH_TEST);
    GLOW_SCOPED(disable, GL_CULL_FACE);

    // sum rgb premul, sum coverage
    auto accumVolumeR = Texture3D::createStorageImmutable(mVolumeSize, mVolumeSize, mVolumeSize, GL_R32I, 1);
    auto accumVolumeG = Texture3D::createStorageImmutable(mVolumeSize, mVolumeSize, mVolumeSize, GL_R32I, 1);
    auto accumVolumeB = Texture3D::createStorageImmutable(mVolumeSize, mVolumeSize, mVolumeSize, GL_R32I, 1);
    auto accumVolumeDX = Texture3D::createStorageImmutable(mVolumeSize, mVolumeSize, mVolumeSize, GL_R32I, 1);
    auto accumVolumeDY = Texture3D::createStorageImmutable(mVolumeSize, mVolumeSize, mVolumeSize, GL_R32I, 1);
    auto accumVolumeDZ = Texture3D::createStorageImmutable(mVolumeSize, mVolumeSize, mVolumeSize, GL_R32I, 1);
    auto accumVolumeA = Texture3D::createStorageImmutable(mVolumeSize, mVolumeSize, mVolumeSize, GL_R32I, 1);
    accumVolumeR->clear<int>(0);
    accumVolumeG->clear<int>(0);
    accumVolumeB->clear<int>(0);
    accumVolumeDX->clear<int>(0);
    accumVolumeDY->clear<int>(0);
    accumVolumeDZ->clear<int>(0);
    accumVolumeA->clear<int>(0);

    // write 3x to volume tex
    {
        // conservative rasterization?
        GLOW_SCOPED(enable, GL_CONSERVATIVE_RASTERIZATION_NV);

        mTexDebugVolX = Texture2D::create(mVolumeSize, mVolumeSize, GL_RGBA);
        mTexDebugVolY = Texture2D::create(mVolumeSize, mVolumeSize, GL_RGBA);
        mTexDebugVolZ = Texture2D::create(mVolumeSize, mVolumeSize, GL_RGBA);

        mTexDebugVolX->clear(glm::vec4(0, 0, 0, 1));
        mTexDebugVolY->clear(glm::vec4(0, 0, 0, 1));
        mTexDebugVolZ->clear(glm::vec4(0, 0, 0, 1));

        auto shader = mShaderGrassVoxelize->use();
        shader.setImage(0, accumVolumeR);
        shader.setImage(1, accumVolumeG);
        shader.setImage(2, accumVolumeB);
        shader.setImage(3, accumVolumeA);
        shader.setImage(4, accumVolumeDX);
        shader.setImage(5, accumVolumeDY);
        shader.setImage(6, accumVolumeDZ);
        shader.setUniform("uGrassDepth", mGrassDepth);
        shader.setUniform("uVoxelWidth", 1.0f / mVolumeSize);
        shader.setUniform("uVolumeSize", (float)mVolumeSize);

        auto dx = glm::vec3(1, 0, 0);
        auto dy = glm::vec3(0, 1, 0);
        auto dz = glm::vec3(0, 0, 1);

        if (mVoxelizeZ)
        {
            auto tmpFramebuffer = Framebuffer::create({{"fColor", mTexDebugVolZ}});
            auto framebuffer = tmpFramebuffer->bind();
            shader.setUniform("uView", glm::mat4(glm::mat3(dx, dy, dz)));
            shader.setUniform("uDepthDir", dz);
            for (auto ddx = 0; ddx <= 1; ++ddx)
                for (auto ddz = 0; ddz <= 1; ++ddz)
                {
                    shader.setUniform("uModel", translate(glm::vec3(ddx, 0, ddz)));
                    mProtoGrass->bind().draw();
                }
        }

        if (mVoxelizeX)
        {
            auto tmpFramebuffer = Framebuffer::create({{"fColor", mTexDebugVolX}});
            auto framebuffer = tmpFramebuffer->bind();
            shader.setUniform("uView", glm::mat4(glm::mat3(dy, dz, dx)));
            shader.setUniform("uDepthDir", dx);
            for (auto ddx = 0; ddx <= 1; ++ddx)
                for (auto ddz = 0; ddz <= 1; ++ddz)
                {
                    shader.setUniform("uModel", translate(glm::vec3(ddx, 0, ddz)));
                    mProtoGrass->bind().draw();
                }
        }

        if (mVoxelizeY)
        {
            auto tmpFramebuffer = Framebuffer::create({{"fColor", mTexDebugVolY}});
            auto framebuffer = tmpFramebuffer->bind();
            shader.setUniform("uView", glm::mat4(glm::mat3(dz, dx, dy)));
            shader.setUniform("uDepthDir", dy);
            for (auto ddx = 0; ddx <= 1; ++ddx)
                for (auto ddz = 0; ddz <= 1; ++ddz)
                {
                    shader.setUniform("uModel", translate(glm::vec3(ddx, 0, ddz)));
                    mProtoGrass->bind().draw();
                }
        }

        mTexDebugVolX->bind().generateMipmaps();
        mTexDebugVolY->bind().generateMipmaps();
        mTexDebugVolZ->bind().generateMipmaps();
    }

    // resolve (16f due to premultiplied)
    mTexGrassVolume = Texture3D::createStorageImmutable(mVolumeSize, mVolumeSize, mVolumeSize, GL_RGBA16F, 1);
    mTexGrassVolumeDirs = Texture3D::createStorageImmutable(mVolumeSize, mVolumeSize, mVolumeSize, GL_RGBA16F, 1);
    {
        auto shader = mShaderVoxelResolve->use();
        shader.setImage(0, accumVolumeR);
        shader.setImage(1, accumVolumeG);
        shader.setImage(2, accumVolumeB);
        shader.setImage(3, accumVolumeA);
        shader.setImage(4, mTexGrassVolume);
        int g = static_cast<int>(glm::ceil(mVolumeSize / 2.f));
        shader.compute(g, g, g);
    }
    {
        auto shader = mShaderVoxelResolveDirs->use();
        shader.setImage(0, accumVolumeDX);
        shader.setImage(1, accumVolumeDY);
        shader.setImage(2, accumVolumeDZ);
        shader.setImage(3, accumVolumeA);
        shader.setImage(4, mTexGrassVolumeDirs);
        int g = static_cast<int>(glm::ceil(mVolumeSize / 2.f));
        shader.compute(g, g, g);
    }


    mTexGrassVolume->bind().setWrapT(GL_CLAMP_TO_EDGE);
    mTexGrassVolumeDirs->bind().setWrapT(GL_CLAMP_TO_EDGE);

    mTexGrassVolume->bind().setMagFilter(GL_LINEAR);
    mTexGrassVolume->bind().setMinFilter(GL_LINEAR);
    mTexGrassVolumeDirs->bind().setMinFilter(GL_LINEAR);

    // TODO: proper directional mipmaps
}

namespace
{
/*! \brief Convert HSV to RGB color space

  See https://gist.github.com/fairlight1337/4935ae72bcbcc1ba5c72

  Converts a given set of HSV values `h', `s', `v' into RGB
  coordinates. The output RGB values are in the range [0, 1], and
  the input HSV values are in the ranges h = [0, 360], and s, v =
  [0, 1], respectively.

  \param fR Red component, used as output, range: [0, 1]
  \param fG Green component, used as output, range: [0, 1]
  \param fB Blue component, used as output, range: [0, 1]
  \param fH Hue component, used as input, range: [0, 360]
  \param fS Hue component, used as input, range: [0, 1]
  \param fV Hue component, used as input, range: [0, 1]

*/
void HSVtoRGB(float& fR, float& fG, float& fB, float fH, float fS, float fV)
{
    fS = glm::clamp(fS, 0.0f, 1.0f);
    fV = glm::clamp(fV, 0.0f, 1.0f);
    float fC = fV * fS; // Chroma
    float fHPrime = fmod(fH / 60.0f, 6.f);
    float fX = fC * (1 - fabs(fmod(fHPrime, 2.f) - 1));
    float fM = fV - fC;

    if (0 <= fHPrime && fHPrime < 1)
    {
        fR = fC;
        fG = fX;
        fB = 0;
    }
    else if (1 <= fHPrime && fHPrime < 2)
    {
        fR = fX;
        fG = fC;
        fB = 0;
    }
    else if (2 <= fHPrime && fHPrime < 3)
    {
        fR = 0;
        fG = fC;
        fB = fX;
    }
    else if (3 <= fHPrime && fHPrime < 4)
    {
        fR = 0;
        fG = fX;
        fB = fC;
    }
    else if (4 <= fHPrime && fHPrime < 5)
    {
        fR = fX;
        fG = 0;
        fB = fC;
    }
    else if (5 <= fHPrime && fHPrime < 6)
    {
        fR = fC;
        fG = 0;
        fB = fX;
    }
    else
    {
        fR = 0;
        fG = 0;
        fB = 0;
    }

    fR += fM;
    fG += fM;
    fB += fM;
}


glm::vec3 randomDir(std::default_random_engine& random)
{
    std::uniform_real_distribution<float> dice(-1, 1);
    while (true)
    {
        auto x = dice(random);
        auto y = dice(random);
        auto z = dice(random);
        if (x * x + y * y + z * z <= 1)
            return normalize(glm::vec3(x, y, z));
    }
}
glm::vec3 randomYDir(std::default_random_engine& random, float angleRange)
{
    std::uniform_real_distribution<float> dice(0, 1);
    auto azi = dice(random) * glm::pi<float>() * 2;
    auto alti = dice(random) * angleRange;

    auto y = cos(alti);
    auto l = sin(alti);
    auto x = cos(azi) * l;
    auto z = sin(azi) * l;

    return {x, y, z};
}

SharedVertexArray genProtoGrass()
{
    std::default_random_engine random;
    std::uniform_real_distribution<float> dice(0, 1);
    std::normal_distribution<float> grassWidth(0.05f, 0.01f);
    std::normal_distribution<float> grassLength(0.6f, 0.3f);

    std::normal_distribution<float> grassHue(90, 10);
    std::normal_distribution<float> grassSaturation(1.0f, 0.1f);
    std::normal_distribution<float> grassValue(1.0f, 0.1f);

    struct Vertex
    {
        glm::vec3 pos;
        glm::vec3 normal;
        glm::vec3 tangent;
        glm::vec3 color;
    };

    auto ab = ArrayBuffer::create();
    ab->defineAttribute(&Vertex::pos, "aPosition");
    ab->defineAttribute(&Vertex::normal, "aNormal");
    ab->defineAttribute(&Vertex::tangent, "aTangent");
    ab->defineAttribute(&Vertex::color, "aColor");

    std::vector<Vertex> vertices;
    for (auto i = 0; i < 500; ++i)
    {
        auto x = dice(random) - .5f;
        auto z = dice(random) - .5f;

        auto dir = randomYDir(random, glm::radians(50.f));
        auto left = normalize(cross(dir, randomDir(random)));
        auto normal = cross(left, dir);

        auto width = grassWidth(random);
        auto length = grassLength(random);

        left.y = 0;
        auto pos = glm::vec3(x, 0, z);
        auto p0 = pos + left * (width / 2.f);
        auto p1 = pos - left * (width / 2.f);
        auto p2 = pos + dir * length;

        float r, g, b;
        HSVtoRGB(r, g, b, grassHue(random), grassSaturation(random), grassValue(random));
        auto color = glm::vec3(r, g, b);

        vertices.push_back({p0, normal, dir, color});
        vertices.push_back({p1, normal, dir, color});
        vertices.push_back({p2, normal, dir, color});
    }
    ab->bind().setData(vertices);

    return VertexArray::create(ab);
}
}

void GrassSample::drawTexture(int x, int y, int w, int h, SharedTexture2D const& tex)
{
    int ww = getWindowWidth();
    int wh = getWindowHeight();
    glm::vec2 from = glm::vec2(x, y) / glm::vec2(ww, wh);
    glm::vec2 to = glm::vec2(x + w, y + h) / glm::vec2(ww, wh);

    auto shader = mShaderDrawTexture->use();

    GLOW_SCOPED(disable, GL_DEPTH_TEST);

    shader.setUniform("uFrom", from);
    shader.setUniform("uTo", to);
    shader.setTexture("uTexture", tex);

    mQuad->bind().draw();
}

void GrassSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    setUsePipeline(true);
    setUsePipelineConfigGui(false);

    GlfwApp::init();

    DefaultShaderParser::addIncludePath(util::pathOf(__FILE__));

    mShaderGround = Program::createFromFile("ground");
    mShaderGrass = Program::createFromFile("grass");
    mShaderGrassVoxelize = Program::createFromFile("grass-voxelize");
    mShaderVoxelResolve = Program::createFromFile("volume-resolve");
    mShaderVoxelResolveDirs = Program::createFromFile("volume-resolve-dir");
    mShaderDebugVolume = Program::createFromFile("debug-volume");
    mShaderDrawTexture = Program::createFromFile("draw-texture");
    mShaderCubeTrace = Program::createFromFile("cube-trace");
    mCube = geometry::Cube<>().generate();
    mQuad = geometry::Quad<>().generate();
    mPlane = assimp::Importer().load(util::pathOf(__FILE__) + "/../../../data/plane.obj");

    // TwAddVarRW(tweakbar(), "Levels", TW_TYPE_INT32, &mLevels, "min=1");
    TwAddVarRW(tweakbar(), "Proto Geometry", TW_TYPE_BOOLCPP, &mDrawProtoGeometry, "group=draw");
    TwAddVarRW(tweakbar(), "Proto Geometry (x4)", TW_TYPE_BOOLCPP, &mDrawProtoGeometry4, "group=draw");
    TwAddVarRW(tweakbar(), "Debug Volume", TW_TYPE_BOOLCPP, &mDrawDebugVolume, "group=draw");
    TwAddVarRW(tweakbar(), "Debug Volume Tex", TW_TYPE_BOOLCPP, &mDrawDebugVolumeTextures, "group=draw");
    TwAddVarRW(tweakbar(), "Cube Trace", TW_TYPE_BOOLCPP, &mDrawCubeTrace, "group=draw");
    TwAddVarRW(tweakbar(), "Ground", TW_TYPE_BOOLCPP, &mDrawGround, "group=draw");

    TwAddVarRW(tweakbar(), "Grass Depth", TW_TYPE_FLOAT, &mGrassDepth, "group=parameters min=0.001 max=1 step=0.001");

    TwAddVarRW(tweakbar(), "Voxelize X", TW_TYPE_BOOLCPP, &mVoxelizeX, "group=voxelize");
    TwAddVarRW(tweakbar(), "Voxelize Y", TW_TYPE_BOOLCPP, &mVoxelizeY, "group=voxelize");
    TwAddVarRW(tweakbar(), "Voxelize Z", TW_TYPE_BOOLCPP, &mVoxelizeZ, "group=voxelize");
    TwAddVarRW(tweakbar(), "Volume Size", TW_TYPE_INT32, &mVolumeSize, "group=voxelize min=1");

    TwAddButton(tweakbar(), "Generate Volume", setGenerateVolume, nullptr, "group=voxelize");

    TwAddVarRW(tweakbar(), "Light Dir", TW_TYPE_DIR3F, &mLightDir, "group=rendering");
    TwAddVarRW(tweakbar(), "Specular Color", TW_TYPE_COLOR3F, &mSpecularColor, "group=rendering");
    TwAddVarRW(tweakbar(), "Specular Exp", TW_TYPE_FLOAT, &mSpecularExponent, "group=rendering min=1 max=128 step=0.1");

    // Generate proto grass
    mProtoGrass = genProtoGrass();

    // Generate volume tex
    GenerateVolume();
}

void GrassSample::render(float dt)
{
    if (volumeGenerationRequested)
    {
        GenerateVolume();
        volumeGenerationRequested = false;
    }

    GlfwApp::render(dt);

    // debug volume textures
    if (mDrawDebugVolumeTextures)
    {
        int w = getWindowWidth();
        int h = getWindowHeight();

        int x = w / 2 - (mVolumeSize * 3 + 10 * 2) / 2;
        int y = h - mVolumeSize;

        drawTexture(x + (mVolumeSize + 10) * 0, y, mVolumeSize, mVolumeSize, mTexDebugVolX);
        drawTexture(x + (mVolumeSize + 10) * 1, y, mVolumeSize, mVolumeSize, mTexDebugVolY);
        drawTexture(x + (mVolumeSize + 10) * 2, y, mVolumeSize, mVolumeSize, mTexDebugVolZ);
    }
}

void GrassSample::onRenderOpaquePass(glow::pipeline::RenderContext const& ctx)
{
    // ground
    if (mDrawGround)
    {
        auto shader = ctx.useProgram(mShaderGround);

        shader.setUniform("uRuntime", static_cast<GLfloat>(glfwGetTime()));
        shader.setUniform("uView", ctx.camData.view);
        shader.setUniform("uProj", ctx.camData.proj);
        shader.setUniform("uModel", glm::mat4());

        shader.setUniform("uLightDir", mLightDir);
        shader.setUniform("uCamPos", ctx.camData.camPos);

        shader.setUniform("uSpecularColor", mSpecularColor);
        shader.setUniform("uSpecularExponent", mSpecularExponent);

        mPlane->bind().draw();
    }

    // proto grass geometry
    if (mDrawProtoGeometry)
    {
        auto shader = ctx.useProgram(mShaderGrass);

        shader.setUniform("uRuntime", static_cast<GLfloat>(glfwGetTime()));
        shader.setUniform("uView", ctx.camData.view);
        shader.setUniform("uProj", ctx.camData.proj);

        shader.setUniform("uLightDir", mLightDir);
        shader.setUniform("uCamPos", ctx.camData.camPos);

        shader.setUniform("uSpecularColor", mSpecularColor);
        shader.setUniform("uSpecularExponent", mSpecularExponent);

        // GLOW_SCOPED(disable, GL_CULL_FACE); // no culling

        if (mDrawProtoGeometry4)
            for (auto dx = 0; dx <= 1; ++dx)
                for (auto dz = 0; dz <= 1; ++dz)
                {
                    shader.setUniform("uModel", translate(glm::vec3(-.5 + dx, 0, -.5 + dz)));
                    mProtoGrass->bind().draw();
                }
        else
            mProtoGrass->bind().draw();
    }

    // cube trace
    if (mDrawCubeTrace)
    {
        GLOW_SCOPED(enable, GL_CULL_FACE);

        auto shader = ctx.useProgram(mShaderCubeTrace);

        shader.setUniform("uView", ctx.camData.view);
        shader.setUniform("uProj", ctx.camData.proj);

        shader.setUniform("uLightDir", mLightDir);
        shader.setUniform("uCamPos", ctx.camData.camPos);

        shader.setUniform("uSpecularColor", mSpecularColor);
        shader.setUniform("uSpecularExponent", mSpecularExponent);

        shader.setTexture("uTexVolume", mTexGrassVolume);
        shader.setTexture("uTexVolumeDirs", mTexGrassVolumeDirs);

        shader.setUniform("uCellWidth", 1.0f / mVolumeSize);
        shader.setUniform("uVolumeResolution", mVolumeSize);
        shader.setUniform("uTextureScale", 1.f);

        auto volScale = glm::vec3(2, .5, 2);
        for (auto const& pos : {glm::vec3(0, .5f, 0), glm::vec3(6, 1.5f, 6), glm::vec3(6, -.5f, 0)})
        {
            shader.setUniform("uModel", translate(pos) * scale(volScale));
            shader.setUniform("uVolumeMin", pos - volScale);
            shader.setUniform("uVolumeMax", pos + volScale);
            mCube->bind().draw();
        }
    }
}

void GrassSample::onRenderTransparentPass(glow::pipeline::RenderContext const& ctx)
{
    // debug volume
    if (mDrawDebugVolume)
    {
        auto shader = ctx.useProgram(mShaderDebugVolume);

        shader.setUniform("uRuntime", static_cast<GLfloat>(glfwGetTime()));
        shader.setUniform("uView", ctx.camData.view);
        shader.setUniform("uProj", ctx.camData.proj);
        shader.setUniform("uModel", glm::mat4());

        shader.setUniform("uLightDir", mLightDir);
        shader.setUniform("uCamPos", ctx.camData.camPos);

        shader.setUniform("uSpecularColor", mSpecularColor);
        shader.setUniform("uSpecularExponent", mSpecularExponent);

        shader.setUniform("uVolumeSize", mVolumeSize);
        shader.setImage(0, mTexGrassVolume);
        shader.setImage(1, mTexGrassVolumeDirs);

        mCube->bind().draw(mVolumeSize * mVolumeSize * mVolumeSize);
    }
}
