in vec3 aPosition;
in vec3 aNormal;
in vec3 aTangent;
in vec3 aColor;

out vec3 vPosition;
out vec3 vNormal;
out vec3 vTangent;
out vec3 vColor;

uniform mat4 uModel;
uniform mat4 uView;

void main() {
    vPosition = vec3(uModel * vec4(aPosition, 1.0));
    vNormal = aNormal;
    vTangent = aTangent;
    vColor = aColor;
    gl_Position = transpose(uView) * vec4(vPosition * 2 - 1, 1.0);
}
