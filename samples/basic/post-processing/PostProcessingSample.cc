#include "PostProcessingSample.hh"

#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/TextureRectangle.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/data/TextureData.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/assimp/Importer.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>

#include <AntTweakBar.h>

#include <GLFW/glfw3.h>

using namespace glow;
using namespace glow::camera;

void PostProcessingSample::copyTo(glow::SharedTextureRectangle const& from, glow::SharedFramebuffer const& to) const
{
    auto shader = mShaderCopy->use();
    shader.setTexture("uTexture", from);

    auto vao = mMeshQuad->bind();

    GLOW_SCOPED(disable, GL_DEPTH_TEST);
    GLOW_SCOPED(disable, GL_CULL_FACE);

    if (to)
    {
        auto fb = to->bind();
        vao.draw();
    }
    else
        vao.draw();
}

void PostProcessingSample::apply(glow::SharedProgram const& program, int w, int h)
{
    auto cam = getCamera();
    auto shader = program->use();

    shader.setUniform("uMotionBlurLength", mMotionBlurLength / 1000.0f);
    shader.setUniform("uVignetteA", mVignetteA);
    shader.setUniform("uVignetteB", mVignetteB);
    shader.setUniform("uGrainStrength", mGrainStrength);
    shader.setUniform("uGrainSpeed", mGrainSpeed);
    shader.setUniform("uGrainSize", mGrainSize);
    shader.setUniform("uOutlineNormal", mOutlineNormal);
    shader.setUniform("uOutlineDepth", mOutlineDepth);
    shader.setUniform("uOutlineColor", mOutlineColor);
    shader.setUniform("uQuantization", (float)mQuantization);
    shader.setUniform("uAberrationStrength", mAberrationStrength);
    shader.setUniform("uSharpen", mSharpen);
    shader.setUniform("uZoomLvl", mZoomLvl);
    shader.setUniform("uMousePos", glm::vec2(mZoomPos.x, getWindowHeight() - mZoomPos.y - 1));

    shader.setUniform("uRuntime", (float)mRuntime);
    shader.setUniform("uElapsedSeconds", mElapsedSeconds);
    shader.setUniform("uCamPos", cam->getPosition());
    shader.setUniform("uView", cam->getViewMatrix());
    shader.setUniform("uProj", cam->getProjectionMatrix());
    shader.setUniform("uInvView", inverse(cam->getViewMatrix()));
    shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
    shader.setUniform("uPrevView", mPrevView);
    shader.setUniform("uPrevProj", mPrevProj);
    shader.setUniform("uInvPrevView", inverse(mPrevView));
    shader.setUniform("uInvPrevProj", inverse(mPrevProj));

    shader.setTexture("uTexColor", mTexColor);
    shader.setTexture("uTexNormal", mTexNormal);
    shader.setTexture("uTexPosition", mTexPosition);
    shader.setTexture("uTexVelocity", mTexVelocity);
    shader.setTexture("uTexDepth", mTexDepth);

    shader.setTexture("uTexture", mTexPostCurr);

    auto fb = mFramebufferToNext->bind();
    if (w != -1 && h != -1)
        glViewport(0, 0, w, h);

    mMeshQuad->bind().draw();

    ppSwap();
}

void PostProcessingSample::ppSwap()
{
    std::swap(mFramebufferToCurr, mFramebufferToNext);
    std::swap(mTexPostCurr, mTexPostNext);
}

void PostProcessingSample::init()
{
    // setUseDefaultCameraHandlingLeft(false);

    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mTexColor = TextureRectangle::create(2, 2, GL_RGBA16F);
    mTexDepth = TextureRectangle::create(2, 2, GL_DEPTH_COMPONENT32);
    mTexNormal = TextureRectangle::create(2, 2, GL_RGBA16F);
    mTexPosition = TextureRectangle::create(2, 2, GL_RGBA32F);
    mTexVelocity = TextureRectangle::create(2, 2, GL_RGBA16F);

    mFramebufferSolid = Framebuffer::create({{"fColor", mTexColor},       //
                                             {"fNormal", mTexNormal},     //
                                             {"fVelocity", mTexVelocity}, //
                                             {"fPosition", mTexPosition}},
                                            mTexDepth);

    auto pbt = util::pathOf(__FILE__) + "/../../../data/cubemap/miramar";
    mTexSkybox = TextureCubeMap::createFromData(TextureData::createFromFileCube( //
        pbt + "/posx.jpg",                                                       //
        pbt + "/negx.jpg",                                                       //
        pbt + "/posy.jpg",                                                       //
        pbt + "/negy.jpg",                                                       //
        pbt + "/posz.jpg",                                                       //
        pbt + "/negz.jpg",                                                       //
        ColorSpace::sRGB));

    mTexPostCurr = TextureRectangle::create(2, 2, GL_RGBA16F);
    mTexPostNext = TextureRectangle::create(2, 2, GL_RGBA16F);

    mFramebufferToCurr = Framebuffer::create("fColor", mTexPostCurr);
    mFramebufferToNext = Framebuffer::create("fColor", mTexPostNext);

    mShaderObject = Program::createFromFile(util::pathOf(__FILE__) + "/shader/obj");

    mShaderBackground = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.bg");
    mShaderCopy = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.copy");

    mShaderDithering = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.dithering");
    mShaderGammaCorrection = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.gamma-correction");
    mShaderFXAA = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.fxaa");
    mShaderZoom = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.zoom");
    mShaderDownsample2 = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.downsample-2");
    mShaderSobel = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.sobel");
    mShaderSharpen = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.sharpen");
    mShaderMotionBlur = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.motion-blur");
    mShaderVignette = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.vignette");
    mShaderGrain = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.grain");
    mShaderChromaticAberrations = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.chromatic-aberration");
    mShaderOutline = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.outline");
    mShaderQuantization = Program::createFromFile(util::pathOf(__FILE__) + "/shader/pp.quantization");

    mMeshQuad = geometry::Quad<>().generate();
    mMeshSuzanne = assimp::Importer().load(util::pathOf(__FILE__) + "/../../../data/suzanne.obj");
    mMeshPlane = assimp::Importer().load(util::pathOf(__FILE__) + "/../../../data/plane.obj");

    auto cam = getCamera();
    cam->setLookAt({2, 3, 2}, {0, 0, 0});

    TwAddVarRW(tweakbar(), "Animate", TW_TYPE_BOOLCPP, &mAnimate, "group=scene");
    TwAddVarRW(tweakbar(), "Speed", TW_TYPE_FLOAT, &mSpeed, "group=scene min=0 max=10 step=0.1");

    TwAddVarRW(tweakbar(), "Light Color", TW_TYPE_COLOR3F, &mLightColor, "group=rendering");
    TwAddVarRW(tweakbar(), "Light Dir", TW_TYPE_DIR3F, &mLightDir, "group=rendering");
    TwAddVarRW(tweakbar(), "Light Distance", TW_TYPE_FLOAT, &mLightDistance, "group=rendering min=1.0 max=100.0");

    TwAddVarRW(tweakbar(), "Quantization", TW_TYPE_INT32, &mQuantization, "group=postprocess min=0 max=100");
    TwAddVarRW(tweakbar(), "Outline Color", TW_TYPE_COLOR3F, &mOutlineColor, "group=postprocess");
    TwAddVarRW(tweakbar(), "Outline", TW_TYPE_BOOLCPP, &mEnableOutline, "group=postprocess");
    TwAddVarRW(tweakbar(), "Outline Normal", TW_TYPE_FLOAT, &mOutlineNormal, "group=postprocess min=0 max=100 step=0.1");
    TwAddVarRW(tweakbar(), "Outline Depth", TW_TYPE_FLOAT, &mOutlineDepth, "group=postprocess min=0 max=100 step=0.1");
    TwAddVarRW(tweakbar(), "Simple Outline", TW_TYPE_BOOLCPP, &mEnableSimpleOutline, "group=postprocess");
    TwAddVarRW(tweakbar(), "Simple Outline Strength", TW_TYPE_FLOAT, &mSimpleOutlineStrength, "group=postprocess min=0 max=1 step=0.01");
    TwAddVarRW(tweakbar(), "Chromatic Aberrations", TW_TYPE_BOOLCPP, &mEnableChromaticAberrations, "group=postprocess");
    TwAddVarRW(tweakbar(), "Aberrations Strength", TW_TYPE_FLOAT, &mAberrationStrength, "group=postprocess min=0 max=100");
    TwAddVarRW(tweakbar(), "Grain", TW_TYPE_BOOLCPP, &mEnableGrain, "group=postprocess");
    TwAddVarRW(tweakbar(), "Grain Strength", TW_TYPE_FLOAT, &mGrainStrength, "group=postprocess min=0 max=1 step=0.01");
    TwAddVarRW(tweakbar(), "Grain Size", TW_TYPE_FLOAT, &mGrainSize, "group=postprocess min=0 max=100 step=1");
    TwAddVarRW(tweakbar(), "Grain Speed", TW_TYPE_FLOAT, &mGrainSpeed, "group=postprocess min=0 max=10 step=0.1");
    TwAddVarRW(tweakbar(), "Vignette", TW_TYPE_BOOLCPP, &mEnableVignette, "group=postprocess");
    TwAddVarRW(tweakbar(), "Vignette Outer", TW_TYPE_FLOAT, &mVignetteA, "group=postprocess min=0 max=10 step=0.01");
    TwAddVarRW(tweakbar(), "Vignette Inner", TW_TYPE_FLOAT, &mVignetteB, "group=postprocess min=0 max=10 step=0.01");
    TwAddVarRW(tweakbar(), "Motion Blur", TW_TYPE_BOOLCPP, &mEnableMotionBlur, "group=postprocess");
    TwAddVarRW(tweakbar(), "Motion Blur Length", TW_TYPE_FLOAT, &mMotionBlurLength, "group=postprocess min=1 max=1000");
    TwAddVarRW(tweakbar(), "Dithering", TW_TYPE_BOOLCPP, &mEnableDithering, "group=postprocess");
    TwAddVarRW(tweakbar(), "FXAA", TW_TYPE_BOOLCPP, &mEnableFXAA, "group=postprocess");
    TwAddVarRW(tweakbar(), "Gamma Correction", TW_TYPE_BOOLCPP, &mEnableGammaCorrection, "group=postprocess");
    TwAddVarRW(tweakbar(), "Sobel Edge Detection", TW_TYPE_BOOLCPP, &mEnableSobel, "group=postprocess");
    TwAddVarRW(tweakbar(), "Sharpen", TW_TYPE_FLOAT, &mSharpen, "group=postprocess min=-10 max=10 step=0.01");

    TwAddVarRW(tweakbar(), "Zoom", TW_TYPE_FLOAT, &mZoomLvl, "group=debug min=1 max=32 step=1");
    TwAddVarRW(tweakbar(), "Downsample", TW_TYPE_INT32, &mDownsampling, "group=debug min=0 max=5 step=1");
    TwAddVarRW(tweakbar(), "Wireframe", TW_TYPE_BOOLCPP, &mWireframe, "group=debug");
    TwAddVarRW(tweakbar(), "Backface Culling", TW_TYPE_BOOLCPP, &mBackfaceCulling, "group=debug");
}

void PostProcessingSample::render(float elapsedSeconds)
{
    auto cam = getCamera();

    // time
    mElapsedSeconds = elapsedSeconds;
    if (mAnimate)
        mRuntime += elapsedSeconds * mSpeed;

    // draw 3D scene
    {
        auto fb = mFramebufferSolid->bind();

        GLOW_SCOPED(clearColor, 0, 0, 0, 1);
        GLOW_SCOPED(enable, GL_DEPTH_TEST);
        GLOW_SCOPED(enable, GL_CULL_FACE);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // bg
        {
            GLOW_SCOPED(disable, GL_DEPTH_TEST);
            GLOW_SCOPED(disable, GL_CULL_FACE);
            auto shader = mShaderBackground->use();

            shader.setUniform("uInvView", inverse(cam->getViewMatrix()));
            shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
            shader.setTexture("uTexture", mTexSkybox);

            mMeshQuad->bind().draw();
        }

        // objects
        {
            GLOW_SCOPED(onlyEnableIf, mBackfaceCulling, GL_CULL_FACE);
            GLOW_SCOPED(polygonMode, mWireframe ? GL_LINE : GL_FILL);

            auto shader = mShaderObject->use();
            shader.setUniform("uView", cam->getViewMatrix());
            shader.setUniform("uProj", cam->getProjectionMatrix());
            shader.setUniform("uLightPos", normalize(mLightDir) * mLightDistance);
            shader.setUniform("uLightColor", mLightColor);
            shader.setUniform("uElapsedSeconds", mElapsedSeconds);
            shader.setUniform("uOutline", false);
            shader.setUniform("uOutlineStrength", mSimpleOutlineStrength);

            if (mEnableSimpleOutline)
            {
                GLOW_SCOPED(cullFace, GL_FRONT);
                shader.setUniform("uOutline", true);
                shader.setUniform("uModel", translate(glm::vec3(0, 1, 0)) * rotate((float)mRuntime, glm::vec3(0, 1, 0)));
                shader.setUniform("uPrevModel", translate(glm::vec3(0, 1, 0)) * rotate((float)mPrevRuntime, glm::vec3(0, 1, 0)));
                shader.setUniform("uColor", mOutlineColor);
                mMeshSuzanne->bind().draw();
                shader.setUniform("uOutline", false);
            }

            {
                shader.setUniform("uModel", translate(glm::vec3(0, 1, 0)) * rotate((float)mRuntime, glm::vec3(0, 1, 0)));
                shader.setUniform("uPrevModel", translate(glm::vec3(0, 1, 0)) * rotate((float)mPrevRuntime, glm::vec3(0, 1, 0)));
                shader.setUniform("uColor", glm::vec3(0.00, 0.33, 0.66));
                mMeshSuzanne->bind().draw();
            }

            {
                auto m = translate(glm::vec3(0, 0, 0)) * scale(glm::vec3(3));
                shader.setUniform("uModel", m);
                shader.setUniform("uPrevModel", m);
                shader.setUniform("uColor", glm::vec3(0.5));
                mMeshPlane->bind().draw();
            }
        }
    }

    // start post processes
    copyTo(mTexColor, mFramebufferToCurr);

    if (mEnableMotionBlur)
        apply(mShaderMotionBlur);

    if (mEnableOutline)
        apply(mShaderOutline);

    if (mEnableSobel)
        apply(mShaderSobel);

    if (mSharpen != 0.0f)
        apply(mShaderSharpen);

    if (mQuantization > 0)
        apply(mShaderQuantization);

    if (mEnableFXAA)
        apply(mShaderFXAA);

    if (mEnableChromaticAberrations)
        apply(mShaderChromaticAberrations);

    if (mEnableGrain)
        apply(mShaderGrain);

    if (mEnableVignette)
        apply(mShaderVignette);

    if (mEnableGammaCorrection)
        apply(mShaderGammaCorrection);

    if (mEnableDithering)
        apply(mShaderDithering);

    // debug
    for (auto i = 0; i < mDownsampling; ++i)
    {
        auto w = getWindowWidth() >> (i + 1);
        auto h = getWindowHeight() >> (i + 1);
        apply(mShaderDownsample2, w, h);
    }

    if (mZoomLvl > 1)
        apply(mShaderZoom);

    // output
    copyTo(mTexPostCurr, nullptr);

    mPrevProj = cam->getProjectionMatrix();
    mPrevView = cam->getViewMatrix();
    mPrevRuntime = mRuntime;
}

void PostProcessingSample::onResize(int w, int h)
{
    GlfwApp::onResize(w, h);

    for (auto const& tex : {mTexColor,    //
                            mTexNormal,   //
                            mTexVelocity, //
                            mTexPosition, //
                            mTexDepth,    //
                            mTexPostNext, //
                            mTexPostCurr})
    {
        auto t = tex->bind();
        t.setMagFilter(GL_LINEAR);
        t.setMinFilter(GL_LINEAR);
        t.resize(w, h);
    }
}

bool PostProcessingSample::onMousePosition(double x, double y)
{
    if (mMouseLeft)
        mZoomPos = glm::vec2(x, y);

    return GlfwApp::onMousePosition(x, y);
}

bool PostProcessingSample::onMouseButton(double x, double y, int button, int action, int mods, int clickCount)
{
    if (GlfwApp::onMouseButton(x, y, button, action, mods, clickCount))
        return true;

    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        mMouseLeft = action != GLFW_RELEASE;
        if (mMouseLeft)
            mZoomPos = glm::vec2(x, y);
    }

    return false;
}
