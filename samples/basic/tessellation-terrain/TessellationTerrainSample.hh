#pragma once

#include <vector>

#include <glm/ext.hpp>

#include <glow/fwd.hh>
#include <glow/gl.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class TessellationTerrainSample : public glow::glfw::GlfwApp
{
public:
    TessellationTerrainSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;

    glow::SharedVertexArray mQuad;

    glm::vec3 mLightDir = normalize(glm::vec3(-.2, 1, .2));
    bool mWireframe = true;
    bool mShowNormals = false;
    bool mShowEdgeLength = false;
    float mEdgeLength = 10.0;
    int mQuads = 32;
    float mQuadSize = 4.0f;

    float mAmplitude = 16.0f;
    float mWavelength = 40.0f;
    float mAmplitudeFactor = 0.49f;
    float mWavelengthFactor = 0.5f;
    int mMaxLevels = 16;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
