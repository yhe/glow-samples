in vec3 aPosition;
in vec3 aColor;

out vec3 vColor;

uniform float uRuntime;

void main()
{
    vColor = aColor;
    gl_Position.xyz = aPosition * cos(uRuntime);
    gl_Position.w = 1.0;
}
