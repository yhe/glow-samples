#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class CubeCursionSample : public glow::glfw::GlfwApp
{
public:
    CubeCursionSample() : GlfwApp(Gui::AntTweakBar) {}

private:
	int mTexSize = 512;
    glow::SharedProgram mShader;
    glow::SharedVertexArray mCube;
	glow::SharedFramebuffer mFramebuffer;
	glow::SharedTexture2D mTexColor;
	glow::SharedTexture2D mTexDepth;

    float mRuntime = 0.0f;
    bool mAnimate = true;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
