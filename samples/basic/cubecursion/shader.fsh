uniform sampler2D uTexture;
uniform bool uDrawColors;

in vec2 vTexCoord;

out vec3 fColor;

void main() 
{
    if (uDrawColors)
        fColor = vec3(vTexCoord, 0.0);
    else
        fColor = texture(uTexture, vTexCoord).rgb;
}
