#pragma once

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

#include <glm/glm.hpp>

class ShadowSample : public glow::glfw::GlfwApp
{
public:
    ShadowSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    int mShadowSize = 512;
    int mLastShadowSize = 512;
    bool shadowOffset = false;  // offsetting to avoid z-fighting
    bool shadowSelf = false;    // self-shadowing
    bool mAnimate = true;
    float mRuntime = 0.0f;
    glm::vec3 mLightPos = {-1, 10, 5};

    // Vertex arrays
    glow::SharedVertexArray mPlane;
    glow::SharedVertexArray mSuzanne;

    // Shader programs
    glow::SharedProgram mShaderPlane;
    glow::SharedProgram mShaderSuzanne;
    glow::SharedProgram mShaderShadow;

    // Frame buffers
    glow::SharedFramebuffer mFrameBufferShadow;

    // Textures
    glow::SharedTextureRectangle mTextureShadow;

    void createShadowFrameBuffer();
protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
