#pragma once

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class FullscreenQuadSample : public glow::glfw::GlfwApp
{
public:
    FullscreenQuadSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mQuad;

    float mRuntime = 0.0f;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
