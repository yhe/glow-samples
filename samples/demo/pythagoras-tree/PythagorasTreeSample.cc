#include "PythagorasTreeSample.hh"


#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>

#include <glow/util/DefaultShaderParser.hh>

#include <glow/common/str_utils.hh>
#include <glow/common/scoped_gl.hh>

#include <glm/ext.hpp>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

namespace
{
struct Vertex
{
    glm::vec3 pos;
    float length;
};

void addQuad(std::vector<int>& indices, int i0, int i1, int i0t, int i1t)
{
    indices.push_back(i0);
    indices.push_back(i1);
    indices.push_back(i1t);

    indices.push_back(i0);
    indices.push_back(i1t);
    indices.push_back(i0t);
}

void generateTree(int i0,
                  int i1,
                  int i2,
                  std::vector<Vertex>& vertices,
                  std::vector<int>& indices,
                  int maxDepth,
                  float heightFactor,
                  float midFactor,
                  float nearFactor,
                  int depth = 0)
{
    if (depth > maxDepth)
        return;

    auto bi = vertices.size();

    auto const& v0 = vertices[i0];
    auto const& v1 = vertices[i1];
    auto const& v2 = vertices[i2];

    auto p0 = v0.pos;
    auto p1 = v1.pos;
    auto p2 = v2.pos;

    auto c = (p0 + p1 + p2) / 3.f;

    auto d01 = p1 - p0;
    auto d02 = p2 - p0;

    auto n = normalize(cross(d02, d01));

    auto l = (distance(p0, p1) + distance(p1, p2) + distance(p2, p0)) / 3.f * heightFactor;

    auto newL = v0.length + l;

    Vertex v0t = {c + (p0 - c) * nearFactor + n * l, newL};
    Vertex v1t = {c + (p1 - c) * nearFactor + n * l, newL};
    Vertex v2t = {c + (p2 - c) * nearFactor + n * l, newL};

    vertices.push_back(v0t);
    vertices.push_back(v1t);
    vertices.push_back(v2t);

    auto i0t = bi + 0;
    auto i1t = bi + 1;
    auto i2t = bi + 2;

    addQuad(indices, i0, i1, i0t, i1t);
    addQuad(indices, i1, i2, i1t, i2t);
    addQuad(indices, i2, i0, i2t, i0t);

    Vertex m = {(p0 + p1 + p2) / 3.f + n * l * midFactor, newL};
    auto im = bi + 3;

    vertices.push_back(m);

    generateTree(i0t, i1t, im, vertices, indices, maxDepth, heightFactor, midFactor, nearFactor, depth + 1);
    generateTree(i1t, i2t, im, vertices, indices, maxDepth, heightFactor, midFactor, nearFactor, depth + 1);
    generateTree(i2t, i0t, im, vertices, indices, maxDepth, heightFactor, midFactor, nearFactor, depth + 1);
}
}



void PythagorasTreeSample::init()
{
    GlfwApp::init();

    DefaultShaderParser::addIncludePath(util::pathOf(__FILE__)); // for abs includes

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");

    getCamera()->setLookAt({3, 7, 5}, {0, 0, 0});

    std::vector<Vertex> vertices = {{mVertex0, 0.f}, {mVertex1, 0.f}, {mVertex2, 0.f}};
    std::vector<int> indices;
    generateTree(0, 1, 2, vertices, indices, mMaxDepth, mHeightFactor, mMidFactor, mNearFactor);

    auto ab = ArrayBuffer::create({{&Vertex::pos, "aPosition"}, {&Vertex::length, "aLength"}});
    ab->bind().setData(vertices);
    auto eab = ElementArrayBuffer::create(indices);
    mTree = VertexArray::create(ab, eab);

}

void PythagorasTreeSample::render(float elapsedSeconds)
{
    mRuntime += elapsedSeconds;

    GLOW_SCOPED(clearColor, 1, 1, 1, 1);
    scoped::enable dt(GL_DEPTH_TEST);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto shader = mShader->use();
    shader.setUniform("uRuntime", mRuntime);
    shader.setUniform("uView", getCamera()->getViewMatrix());
    shader.setUniform("uProj", getCamera()->getProjectionMatrix());
    shader.setUniform("uModel", glm::rotate(mRuntime, glm::vec3{0, 1, 0}));

    auto tree = mTree->bind();
    tree.draw();
}
