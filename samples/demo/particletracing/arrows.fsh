#glow pipeline opaque

#include <glow-pipeline/pass/opaque/opaquePass.glsl>

in vec2 vTexcoord;

uniform sampler2D uTexture;
uniform vec4 uColor = vec4(1,1,1,1);


void main()
{
    float texAlpha = texture(uTexture, vTexcoord).a;

    texAlpha = pow(texAlpha, 0.3);

    if(texAlpha < 0.1)
        discard;

    outputOpaqueGeometry(uColor.rgb);
}
