#glow pipeline opaque

#include "shader-header.glsl"
#include <glow-pipeline/pass/opaque/opaquePass.glsl>

uniform float uCrustSize;

in vec3 vPosition;

vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() 
{
    ivec3 is = imageSize(uTexNearestPoint);
    ivec3 ip = ivec3(round(vPosition * vec3(is-1)));
    uint np = imageLoad(uTexNearestPoint, ip).r;

    if (np == UDISTANCE_MAX)
        discard;
       
    vec3 p = points[np].xyz;
    float dis = distance(ip, p);

    if (dis > uCrustSize)
        discard;

    // fColor = p / vec3(is); // DEBUG
    // Color = hsv2rgb(vec3(dis / 20, 1.0, 1.0));
    outputOpaqueGeometry(hsv2rgb(vec3(dis / 20, 1.0, 1.0)));
}
