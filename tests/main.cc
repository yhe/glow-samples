#include <doctest.hh>

#include <glow-extras/glfw/GlfwContext.hh>

int main(int argc, char **argv)
{
    glow::glfw::GlfwContext ctx;

    doctest::Context context;
    context.applyCommandLine(argc, argv);

    auto res = context.run();

    return res;
}
