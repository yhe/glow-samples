#include <doctest.hh>

#include <vector>
#include <array>

#include <glm/glm.hpp>
#include <typed-geometry/tg.hh>

#include <glow-extras/viewer/traits.hh>

TEST_CASE("glow-viewer type traits should work with glm, tg, and others")
{
    using namespace glow::viewer::detail;

    // std
    {
        // some true checks
        static_assert(is_pos2_like<std::array<float, 2>>, "");
        static_assert(is_pos3_like<std::array<float, 3>>, "");
        static_assert(is_pos4_like<std::array<float, 4>>, "");

        static_assert(is_pos2_like<std::array<double, 2>>, "");
        static_assert(is_pos3_like<std::array<double, 3>>, "");
        static_assert(is_pos4_like<std::array<double, 4>>, "");

        // some false checks
        static_assert(!is_color3_like<std::array<float, 3>>, "");
        static_assert(!is_color4_like<std::array<float, 4>>, "");
    }

    // glm
    {
        // some true checks
        static_assert(is_pos2_like<glm::vec2>, "");
        static_assert(is_pos3_like<glm::vec3>, "");
        static_assert(is_pos4_like<glm::vec4>, "");

        static_assert(is_pos2_like<glm::dvec2>, "");
        static_assert(is_pos3_like<glm::dvec3>, "");
        static_assert(is_pos4_like<glm::dvec4>, "");

        // questionable but currently valid:
        static_assert(is_color3_like<glm::vec3>, "");
        static_assert(is_color4_like<glm::vec4>, "");

        // some false checks
        static_assert(!is_pos3_like<glm::vec2>, "");
        static_assert(!is_pos3_like<glm::dvec2>, "");
        static_assert(!is_pos3_like<glm::vec4>, "");
        static_assert(!is_color3_like<glm::vec4>, "");
    }

    // tg
    {
        // some true checks
        static_assert(is_pos2_like<tg::vec2>, "");
        static_assert(is_pos3_like<tg::vec3>, "");
        static_assert(is_pos4_like<tg::vec4>, "");

        static_assert(is_pos2_like<tg::dvec2>, "");
        static_assert(is_pos3_like<tg::dvec3>, "");
        static_assert(is_pos4_like<tg::dvec4>, "");

        static_assert(is_pos2_like<tg::pos2>, "");
        static_assert(is_pos3_like<tg::pos3>, "");
        static_assert(is_pos4_like<tg::pos4>, "");

        static_assert(is_pos2_like<tg::dpos2>, "");
        static_assert(is_pos3_like<tg::dpos3>, "");
        static_assert(is_pos4_like<tg::dpos4>, "");

        // TODO:
        // static_assert(is_color3_like<tg::color3>, "");
        // static_assert(is_color4_like<tg::color4>, "");

        // some false checks
        static_assert(!is_color3_like<tg::vec3>, "");
        static_assert(!is_color4_like<tg::vec4>, "");
        static_assert(!is_pos3_like<tg::vec2>, "");
        static_assert(!is_pos3_like<tg::dvec2>, "");
        static_assert(!is_pos3_like<tg::vec4>, "");
        static_assert(!is_color3_like<tg::vec4>, "");

        static_assert(!is_color3_like<tg::pos3>, "");
        static_assert(!is_color4_like<tg::pos4>, "");
        static_assert(!is_pos3_like<tg::pos2>, "");
        static_assert(!is_pos3_like<tg::dpos2>, "");
        static_assert(!is_pos3_like<tg::pos4>, "");
        static_assert(!is_color3_like<tg::pos4>, "");
    }
}
