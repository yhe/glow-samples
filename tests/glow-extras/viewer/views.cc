#include <doctest.hh>

#include <array>
#include <vector>

#include <glm/glm.hpp>
#include <typed-geometry/tg.hh>

#include <glow-extras/viewer/view.hh>

static constexpr bool dont_actually_show = true;

TEST_CASE("view(...) for meshes")
{
    pm::Mesh m;
    auto glm_pos = m.vertices().make_attribute<glm::vec3>();
    auto tg_pos = m.vertices().make_attribute<tg::pos3>();

    if (dont_actually_show)
        return;

    gv::view(glm_pos);
    gv::view(tg_pos);

    gv::view(gv::polygons(glm_pos));
    gv::view(gv::polygons(tg_pos));

    gv::view(gv::points(glm_pos));
    gv::view(gv::points(tg_pos));

    gv::view(gv::lines(glm_pos));
    gv::view(gv::lines(tg_pos));
}

TEST_CASE("view(...) for point clouds")
{
    std::vector<glm::vec3> glm_pts;
    std::vector<tg::pos3> tg_pts;

    if (dont_actually_show)
        return;

    gv::view(glm_pts);
    gv::view(tg_pts);
}
