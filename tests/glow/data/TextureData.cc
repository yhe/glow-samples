#include <doctest.hh>

#include <glow/common/str_utils.hh>
#include <glow/data/SurfaceData.hh>
#include <glow/data/TextureData.hh>

using namespace glow;

TEST_CASE("TextureData, STB")
{
    for (std::string const& file : {"test-rgb.bmp",  //
                                    "test-rgb.jpg",  //
                                    "test-rgb.jpeg", //
                                    "test-rgb.ppm",  //
                                    "test-rgb.tga",  //
                                    "test-grey.bmp", //
                                    "test-grey.jpg", //
                                    "test-grey.tga"})
    {
        auto tex = TextureData::createFromFile(util::pathOf(__FILE__) + "/textures/" + file, ColorSpace::Linear);
        CHECK(tex != nullptr);
        CHECK(tex->getWidth() == 3);
        CHECK(tex->getHeight() == 2);
        CHECK(tex->getSurfaces().size() == 1u);
        auto surf = tex->getSurfaces()[0];
        CHECK(surf->getMipmapLevel() == 0);
        CHECK(surf->getWidth() == 3);
        CHECK(surf->getHeight() == 2);
        CHECK(surf->getType() == (GLenum)GL_UNSIGNED_BYTE);
        CHECK(surf->getFormat() == (GLenum)GL_RGB);

        auto surfA = surf->convertTo(GL_RGBA);
        CHECK(surfA->getMipmapLevel() == 0);
        CHECK(surfA->getWidth() == 3);
        CHECK(surfA->getHeight() == 2);
        CHECK(surfA->getType() == (GLenum)GL_UNSIGNED_BYTE);
        CHECK(surfA->getFormat() == (GLenum)GL_RGBA);
        for (auto i = 0; i < surf->getWidth() * surf->getHeight(); ++i)
        {
            CHECK(surf->getData()[i * 3 + 0] == surfA->getData()[i * 4 + 0]);
            CHECK(surf->getData()[i * 3 + 1] == surfA->getData()[i * 4 + 1]);
            CHECK(surf->getData()[i * 3 + 2] == surfA->getData()[i * 4 + 2]);
            CHECK((char)0xFF == surfA->getData()[i * 4 + 3]);
        }
    }

    for (std::string const& file : {"test-rgba.tga", //
                                    "test-rgb.gif"})
    {
        auto tex = TextureData::createFromFile(util::pathOf(__FILE__) + "/textures/" + file, ColorSpace::Linear);
        CHECK(tex != nullptr);
        CHECK(tex->getWidth() == 3);
        CHECK(tex->getHeight() == 2);
        CHECK(tex->getSurfaces().size() == 1u);
        auto surf = tex->getSurfaces()[0];
        CHECK(surf->getMipmapLevel() == 0);
        CHECK(surf->getWidth() == 3);
        CHECK(surf->getHeight() == 2);
        CHECK(surf->getType() == (GLenum)GL_UNSIGNED_BYTE);
        CHECK(surf->getFormat() == (GLenum)GL_RGBA);

        auto surfA = surf->convertTo(GL_RG);
        CHECK(surfA->getMipmapLevel() == 0);
        CHECK(surfA->getWidth() == 3);
        CHECK(surfA->getHeight() == 2);
        CHECK(surfA->getType() == (GLenum)GL_UNSIGNED_BYTE);
        CHECK(surfA->getFormat() == (GLenum)GL_RG);
        for (auto i = 0; i < surf->getWidth() * surf->getHeight(); ++i)
        {
            CHECK(surf->getData()[i * 4 + 0] == surfA->getData()[i * 2 + 0]);
            CHECK(surf->getData()[i * 4 + 1] == surfA->getData()[i * 2 + 1]);
        }

        auto surfB = surfA->convertTo(GL_RGB);
        CHECK(surfB->getMipmapLevel() == 0);
        CHECK(surfB->getWidth() == 3);
        CHECK(surfB->getHeight() == 2);
        CHECK(surfB->getType() == (GLenum)GL_UNSIGNED_BYTE);
        CHECK(surfB->getFormat() == (GLenum)GL_RGB);
        for (auto i = 0; i < surf->getWidth() * surf->getHeight(); ++i)
        {
            CHECK(surfB->getData()[i * 3 + 0] == surfA->getData()[i * 2 + 0]);
            CHECK(surfB->getData()[i * 3 + 1] == surfA->getData()[i * 2 + 1]);
            CHECK(surfB->getData()[i * 3 + 2] == 0x00);
        }

        auto surfC = surfA->convertTo(GL_RGBA);
        CHECK(surfC->getMipmapLevel() == 0);
        CHECK(surfC->getWidth() == 3);
        CHECK(surfC->getHeight() == 2);
        CHECK(surfC->getType() == (GLenum)GL_UNSIGNED_BYTE);
        CHECK(surfC->getFormat() == (GLenum)GL_RGBA);
        for (auto i = 0; i < surf->getWidth() * surf->getHeight(); ++i)
        {
            CHECK(surfC->getData()[i * 4 + 0] == surfA->getData()[i * 2 + 0]);
            CHECK(surfC->getData()[i * 4 + 1] == surfA->getData()[i * 2 + 1]);
            CHECK(surfC->getData()[i * 4 + 2] == 0x00);
            CHECK(surfC->getData()[i * 4 + 3] == (char)0xFF);
        }
    }
}

TEST_CASE("TextureData, LodePNG")
{
    for (std::string const& file : {"test-rgb.png",  //
                                    "test-rgba.png", //
                                    "test-grey.png"})
    {
        auto tex = TextureData::createFromFile(util::pathOf(__FILE__) + "/textures/" + file, ColorSpace::Linear);
        CHECK(tex != nullptr);
        CHECK(tex->getWidth() == 3);
        CHECK(tex->getHeight() == 2);
        CHECK(tex->getSurfaces().size() == 1u);
        auto surf = tex->getSurfaces()[0];
        CHECK(surf->getMipmapLevel() == 0);
        CHECK(surf->getWidth() == 3);
        CHECK(surf->getHeight() == 2);
        CHECK(surf->getType() == (GLenum)GL_UNSIGNED_BYTE);
        CHECK(surf->getFormat() == (GLenum)GL_RGBA);
    }
}
