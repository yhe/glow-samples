#include <doctest.hh>

#include <glow/gl.hh>
#include <glow/glow.hh>

TEST_CASE("Glow, Version")
{
    CHECK(GLVersion.major >= 4);
    CHECK(GLVersion.minor >= 3);
}
