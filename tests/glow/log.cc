#include <doctest.hh>

#include <sstream>

#include <glow/common/log.hh>

#include <glm/ext.hpp>
#include <typed-geometry/tg-std.hh>

namespace
{
struct Foo1
{
};
std::ostream& operator<<(std::ostream& os, Foo1 const&)
{
    return os;
}

struct Foo2
{
};
std::string to_string(Foo2 const&)
{
    return "";
}
}

TEST_CASE("glow::log usages")
{
    std::stringstream ss;
    glow::setLogStream(&ss, &ss);

    glow::info() << 0;
    glow::info() << "hello world";
    glow::info() << glm::vec3();
    glow::info() << glm::mat4();
    glow::info() << tg::pos3();
    glow::info() << tg::mat4();
    glow::info() << std::string("hello world");
    glow::info() << Foo1();
    glow::info() << Foo2();

    ss << Foo1() << to_string(Foo2()); // to silence warning

    glow::setLogStream(nullptr, nullptr);
}
