#include <doctest.hh>

#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureRectangle.hh>

using namespace glow;

TEST_CASE("Framebuffer, Binding")
{
    auto fbo0 = Framebuffer::create();
    CHECK(Framebuffer::getCurrentBuffer() == nullptr);

    {
        auto bfbo0 = fbo0->bind();
        CHECK(Framebuffer::getCurrentBuffer() == &bfbo0);

        auto fbo1 = Framebuffer::create();
        auto fbo2 = Framebuffer::create();
        CHECK(Framebuffer::getCurrentBuffer() == &bfbo0);

        {
            auto bfbo1 = fbo1->bind();
            CHECK(Framebuffer::getCurrentBuffer() == &bfbo1);

            auto bfbo2 = fbo2->bind();
            CHECK(Framebuffer::getCurrentBuffer() == &bfbo2);
        }

        CHECK(Framebuffer::getCurrentBuffer() == &bfbo0);
    }

    CHECK(Framebuffer::getCurrentBuffer() == nullptr);
}

TEST_CASE("Framebuffer, Attach")
{
    auto tex1 = Texture2D::create();
    auto tex2 = Texture2D::create();
    auto fbo = Framebuffer::create();
    auto bfbo = fbo->bind();
    bfbo.attachColor("A", tex1);
    bfbo.attachColor("B", tex2);

    CHECK(bfbo.checkComplete());
}

TEST_CASE("Framebuffer, AttachCtor")
{
    auto tex1 = Texture2D::create();
    auto tex2 = Texture2D::create();
    auto fbo = Framebuffer::create({{"A", tex1}, {"B", tex2}});
    auto bfbo = fbo->bind();

    CHECK(bfbo.checkComplete());
}

TEST_CASE("Framebuffer, StorageImmutable")
{
    auto tex = TextureRectangle::createStorageImmutable(4, 4, GL_RGBA32F);
    auto fbo = Framebuffer::create();
    auto bfbo = fbo->bind();
    bfbo.attachColor("A", tex);

    CHECK(bfbo.checkComplete());
}
