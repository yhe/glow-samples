#include <doctest.hh>

#include <glow/objects/ElementArrayBuffer.hh>

using namespace glow;

TEST_CASE("ElementArrayBuffer, Binding")
{
    auto eab0 = ElementArrayBuffer::create();
    CHECK(ElementArrayBuffer::getCurrentBuffer() == nullptr);

    {
        auto beab0 = eab0->bind();
        CHECK(ElementArrayBuffer::getCurrentBuffer() == &beab0);

        auto eab1 = ElementArrayBuffer::create();
        auto eab2 = ElementArrayBuffer::create();
        CHECK(ElementArrayBuffer::getCurrentBuffer() == &beab0);

        {
            auto beab1 = eab1->bind();
            CHECK(ElementArrayBuffer::getCurrentBuffer() == &beab1);

            auto beab2 = eab2->bind();
            CHECK(ElementArrayBuffer::getCurrentBuffer() == &beab2);
        }

        CHECK(ElementArrayBuffer::getCurrentBuffer() == &beab0);
    }

    CHECK(ElementArrayBuffer::getCurrentBuffer() == nullptr);
}
