#include <doctest.hh>

#include <glm/glm.hpp>

#include <glow/objects/ArrayBuffer.hh>

using namespace glow;

TEST_CASE("ArrayBuffer, Binding")
{
    auto ab0 = ArrayBuffer::create();
    CHECK(ArrayBuffer::getCurrentBuffer() == nullptr);

    {
        auto bab0 = ab0->bind();
        CHECK(ArrayBuffer::getCurrentBuffer() == &bab0);

        auto ab1 = ArrayBuffer::create();
        auto ab2 = ArrayBuffer::create();
        CHECK(ArrayBuffer::getCurrentBuffer() == &bab0);

        {
            auto bab1 = ab1->bind();
            CHECK(ArrayBuffer::getCurrentBuffer() == &bab1);

            auto bab2 = ab2->bind();
            CHECK(ArrayBuffer::getCurrentBuffer() == &bab2);
        }

        CHECK(ArrayBuffer::getCurrentBuffer() == &bab0);
    }

    CHECK(ArrayBuffer::getCurrentBuffer() == nullptr);
}

TEST_CASE("ArrayBuffer, Attributes")
{
    auto ab = ArrayBuffer::create();

    ab->defineAttribute("a", GL_FLOAT, 3);
    ab->defineAttribute<glm::vec3>("a");
    ab->defineAttribute<float>("a");
    ab->defineAttribute<int>("a");
    ab->defineAttribute<glm::ivec4>("a");

    ab->defineAttributeWithPadding("a", GL_FLOAT, 3, 7);
    ab->defineAttributeWithPadding<glm::vec3>("a", 1);
    ab->defineAttributeWithPadding<float>("a", 2);
    ab->defineAttributeWithPadding<int>("a", 3);
    ab->defineAttributeWithPadding<glm::ivec4>("a", 4);

    ab->defineAttributeWithOffset("a", GL_FLOAT, 3, 7);
    ab->defineAttributeWithOffset<glm::vec3>("a", 1);
    ab->defineAttributeWithOffset<float>("a", 2);
    ab->defineAttributeWithOffset<int>("a", 3);
    ab->defineAttributeWithOffset<glm::ivec4>("a", 4);

    CHECK(ab->getAttributes().size() == 15u);
    CHECK(ab->getStride() == (3 + 3 + 1 + 1 + 4) * 2 * sizeof(float) + 7 + 1 + 2 + 3 + 4);
}

TEST_CASE("ArrayBuffer, AttributeByType")
{
    auto ab = ArrayBuffer::create();

    struct Vertex
    {
        glm::vec3 pos;
        float f;
    };

    ab->defineAttribute<glm::vec3>("pos");
    ab->defineAttribute<float>("f");

    Vertex vertices[] = {
        {{1, 2, 3}, 4.f}, {{5, 6, 7}, 8.f},
    };

    ab->bind().setData(vertices);
}

TEST_CASE("ArrayBuffer, AttributeByStructTemplate")
{
    auto ab = ArrayBuffer::create();

    struct Vertex
    {
        glm::vec3 pos;
        int f;
        double d;
    };

    ab->defineAttribute(&Vertex::pos, "aPosition");
    ab->defineAttribute(&Vertex::d, "aD");
    ab->defineAttribute(&Vertex::f, "aF");

    CHECK(ab->getStride() == sizeof(float) * 6);
    CHECK(ab->getAttributes().size() == 3u);
    CHECK(ab->getAttributes()[0].offset == sizeof(float) * 0);
    CHECK(ab->getAttributes()[1].offset == sizeof(float) * 4);
    CHECK(ab->getAttributes()[2].offset == sizeof(float) * 3);

    Vertex vertices[] = {
        {{1, 2, 3}, 4, .1}, {{5, 6, 7}, 8, .2},
    };

    auto boundAB = ab->bind();
    boundAB.setData(vertices);
}

TEST_CASE("ArrayBuffer, AttributeByMemberList")
{
    auto ab = ArrayBuffer::create();

    struct Vertex
    {
        glm::vec3 pos;
        glm::vec3 normal;
        glm::vec2 tex;
        char _padding;
    };

    ab->defineAttributes({{&Vertex::pos, "aPosition"},  //
                          {&Vertex::normal, "aNormal"}, //
                          {&Vertex::tex, "aTexCoord"}});

    CHECK(ab->getStride() == sizeof(float) * (3 + 3 + 2 + 1));
    CHECK(ab->getAttributes().size() == 3u);
    CHECK(ab->getAttributes()[0].offset == sizeof(float) * 0);
    CHECK(ab->getAttributes()[1].offset == sizeof(float) * 3);
    CHECK(ab->getAttributes()[2].offset == sizeof(float) * 6);

    Vertex vertices[] = {
        {{1, 2, 3}, {0, 1, 2}, {0, 1}}, //
        {{1, 2, 3}, {0, 1, 2}, {0, 1}}, //
    };

    auto boundAB = ab->bind();
    boundAB.setData(vertices);
}
