#include <doctest.hh>

#include <glm/glm.hpp>

#include <glow/objects/Texture2D.hh>

using namespace glow;

TEST_CASE("Texture2D, Data")
{
    auto tex = Texture2D::create();
    auto t = tex->bind();

    glm::ivec4 d[] = {{1, 2, 3, 4}};
    t.setData(GL_RGBA32I, 1, 1, d);
    CHECK(t.getData<glm::ivec4>()[0] == glm::ivec4(1, 2, 3, 4));

    t.setData<uint8_t>(GL_R8, 4, 2, {1, 2, 3, 4, 5, 6, 7, 8});
    CHECK(t.getData<uint8_t>() == std::vector<uint8_t>({1, 2, 3, 4, 5, 6, 7, 8}));

    t.setData<uint8_t>(GL_R8, 1, 8, {1, 2, 3, 4, 5, 6, 7, 8});
    CHECK(t.getData<uint8_t>() == std::vector<uint8_t>({1, 2, 3, 4, 5, 6, 7, 8}));

    t.setData<uint8_t>(GL_R8, 2, 4, {1, 2, 3, 4, 5, 6, 7, 8});
    CHECK(t.getData<uint8_t>() == std::vector<uint8_t>({1, 2, 3, 4, 5, 6, 7, 8}));

    t.setData<uint8_t>(GL_R8, 8, 1, {1, 2, 3, 4, 5, 6, 7, 8});
    CHECK(t.getData<uint8_t>() == std::vector<uint8_t>({1, 2, 3, 4, 5, 6, 7, 8}));
    float img[][3] = {
        {1.f, 2.f, 3.f},    // row 0
        {-1.f, -2.f, -3.f}, // row 1
    };
    t.setData(GL_R32F, img);
    CHECK(t.getData<float>() == std::vector<float>({1, 2, 3, -1, -2, -3}));
}

TEST_CASE("Texture2D, DataImmutable")
{
    auto tex = Texture2D::createStorageImmutable(4, 2, GL_R8);
    auto t = tex->bind();

    t.setData<uint8_t>(GL_R8, 4, 2, {1, 2, 3, 4, 5, 6, 7, 8});
    CHECK(t.getData<uint8_t>() == std::vector<uint8_t>({1, 2, 3, 4, 5, 6, 7, 8}));
}

TEST_CASE("Texture2D, FormatResize")
{
    Texture2D::create(2, 2, GL_R8UI);
    Texture2D::create(2, 2, GL_RG32I);
    Texture2D::create(2, 2, GL_RGB16F);
    Texture2D::create(2, 2, GL_RGBA8);
}
