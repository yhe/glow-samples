// This file is auto-generated and should not be modified directly.
#include <doctest.hh>

#include <glm/glm.hpp>

#include <glow/objects/Texture2DMultisample.hh>

using namespace glow;

TEST_CASE("Texture2DMultisample, Binding")
{
    auto tex0 = Texture2DMultisample::create();
    CHECK(Texture2DMultisample::getCurrentTexture() == nullptr);

    {
        auto btex0 = tex0->bind();
        CHECK(Texture2DMultisample::getCurrentTexture() == &btex0);

        auto tex1 = Texture2DMultisample::create();
        auto tex2 = Texture2DMultisample::create();
        CHECK(Texture2DMultisample::getCurrentTexture() == &btex0);

        {
            auto btex1 = tex1->bind();
            CHECK(Texture2DMultisample::getCurrentTexture() == &btex1);

            auto btex2 = tex2->bind();
            CHECK(Texture2DMultisample::getCurrentTexture() == &btex2);
        }

        CHECK(Texture2DMultisample::getCurrentTexture() == &btex0);
    }

    CHECK(Texture2DMultisample::getCurrentTexture() == nullptr);
}
