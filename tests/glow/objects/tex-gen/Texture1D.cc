// This file is auto-generated and should not be modified directly.
#include <doctest.hh>

#include <glm/glm.hpp>

#include <glow/objects/Texture1D.hh>

using namespace glow;

TEST_CASE("Texture1D, Binding")
{
    auto tex0 = Texture1D::create();
    CHECK(Texture1D::getCurrentTexture() == nullptr);

    {
        auto btex0 = tex0->bind();
        CHECK(Texture1D::getCurrentTexture() == &btex0);

        auto tex1 = Texture1D::create();
        auto tex2 = Texture1D::create();
        CHECK(Texture1D::getCurrentTexture() == &btex0);

        {
            auto btex1 = tex1->bind();
            CHECK(Texture1D::getCurrentTexture() == &btex1);

            auto btex2 = tex2->bind();
            CHECK(Texture1D::getCurrentTexture() == &btex2);
        }

        CHECK(Texture1D::getCurrentTexture() == &btex0);
    }

    CHECK(Texture1D::getCurrentTexture() == nullptr);
}
