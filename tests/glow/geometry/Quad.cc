#include <doctest.hh>

#include <functional>

#include <glm/glm.hpp>

#include <glow-extras/geometry/Quad.hh>

using namespace glow;

static tg::pos2 MakeQuad(float u, float v) { return {u + 1, v + 1}; }

TEST_CASE("ExtrasGeometry, Quad")
{
    struct QuadVertex
    {
        float u;
        float v;
    };

    struct ComplexQuadVertex
    {
        static std::vector<ArrayBufferAttribute> attributes()
        {
            return {
                {&ComplexQuadVertex::i, "i"}, //
                {&ComplexQuadVertex::d, "d"}, //
                {&ComplexQuadVertex::p, "p"},
            };
        }

        int i;
        double d;
        glm::vec4 p;

        ComplexQuadVertex(float u, float v) : i(int(u + v)), d(u - v), p(u, v, u, v) {}
    };

    struct CustomCtorQuadVertex
    {
        glm::vec4 pos;
        CustomCtorQuadVertex(float u, float v) : pos(u, v, 0, 1) {}
    };

    float s = 1.2345f;
    auto make_quad = [&](float u, float v) -> ComplexQuadVertex { return {v + s, u + s}; };
    std::function<tg::pos2(float, float)> make_quad2 = [](float u, float v) -> tg::pos2 { return {v, u}; };

    auto quad0 = geometry::Quad<>().generate();
    auto quad1 = geometry::Quad<tg::pos2>().generate();
    auto quad2 = geometry::Quad<QuadVertex>({{&QuadVertex::u, "u"}, {&QuadVertex::v, "v"}}).generate();
    auto quad3 = geometry::Quad<CustomCtorQuadVertex>({{&CustomCtorQuadVertex::pos, "aPosition"}}).generate();
    auto quad4 = geometry::Quad<ComplexQuadVertex>().generate();
    auto quad5 = geometry::Quad<>().generate([](float u, float v) { return tg::pos2{v, u}; });
    auto quad6 = geometry::Quad<ComplexQuadVertex>().generate(make_quad);
    auto quad7 = geometry::Quad<>().generate(make_quad2);
    auto quad8 = geometry::Quad<>().generate(MakeQuad);
}
